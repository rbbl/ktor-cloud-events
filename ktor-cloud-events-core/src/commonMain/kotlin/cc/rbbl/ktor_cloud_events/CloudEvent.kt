package cc.rbbl.ktor_cloud_events

import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

data class CloudEvent<out DATA : Any>(
    val attributes: Map<String, String>,
    val data: DATA
) {
    fun getAttributeAsBooleanOrNull(key: String): Boolean? = attributes[key]?.toBooleanStrictOrNull()
    fun getAttributeAsIntegerOrNull(key: String): Int? = attributes[key]?.toInt()
    fun getAttributeAsStringOrNull(key: String): String? = attributes[key]

    @OptIn(ExperimentalEncodingApi::class)
    fun getAttributeAsBiteArrayOrNull(key: String): ByteArray? = attributes[key]?.let { Base64.decode(it) }
}
