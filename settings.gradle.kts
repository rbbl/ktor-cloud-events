plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}
rootProject.name = "ktor-cloud-events"
include("ktor-cloud-events-core", "ktor-cloud-events-client")
